#!/bin/bash
set -euxo pipefail

pushd merge
    ./prepare.sh
    ./create-rc-file.sh
    ./clone-repo.sh
    ./get-tag.sh
    ./detect-kernel-type.sh
    ./patches.sh
    ./update-rc-file.sh
    ./make-source-tarball.sh
    ./make-source-rpm.sh
popd
