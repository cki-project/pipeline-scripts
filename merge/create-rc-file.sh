#!/bin/bash
set -euxo pipefail

# Write initial data to the rc_merge file.
if [ ! -f "${RC_FILE}" ]; then
    echo "[state]" > "${RC_FILE}"
fi
echo "workdir = ${WORKDIR}" >> "${RC_FILE}"
echo "git_url = ${git_url}" >> "${RC_FILE}"

# Mark the merge stage as failed for now and replace it with a successful
# status if we make it all the way through the stage later.
echo "stage_merge = fail" >> "${RC_FILE}"

# Some pipelines use a secured patchwork instance which requires a valid
# cookie to download patches.
patchwork_session_cookie=${patchwork_session_cookie:-}
if [ -n "${patchwork_session_cookie}" ]; then
    export CURL_COOKIES="--cookie sessionid=${patchwork_session_cookie}"
else
    export CURL_COOKIES=""
fi

# Add a merge log to the rc file here in case the merge fails. If the merge
# is successful, we will remove this line so the reporter doesn't think that
# the merge failed.
echo "mergelog = ${WORKDIR}/merge.log" >> "${RC_FILE}"

# Record which make target we are using to build this kernel.
echo "make_target = ${make_target}" | tee -a "${RC_FILE}"
