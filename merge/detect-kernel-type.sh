#!/bin/bash
set -euxo pipefail

# 🛂 Detect RHEL or upstream kernel.
export KERNEL_TYPE=upstream
pushd "${WORKDIR}"
    if [ -d 'redhat' ]; then
        export KERNEL_TYPE=redhat
    fi
popd

# Update the rc file.
echo "kernel_type = ${KERNEL_TYPE}" >> "${RC_FILE}"
echo "Found ${KERNEL_TYPE} type kernel"
