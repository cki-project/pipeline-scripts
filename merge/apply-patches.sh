#!/bin/bash
set -euxo pipefail

# Apply patches.
pushd "${WORKDIR}"

    if [ -n "${patch_urls:-}" ]; then
        COUNTER=0
        for patch in ${patch_urls}; do
            if [ -z "${patchwork_url:-}" ]; then
                # Use the last part of URL as filename for non-Patchwork patches.
                PATCH_FILENAME=$(basename "${patch}")
            else
                # All Patchwork URLs and with "mbox/" which is no fun
                PATCH_FILENAME="patch_${COUNTER}.patch"
            fi

            # Download the patch and attempt to apply it.
            curl -s --retry 5 "${CURL_COOKIES}" --connect-timeout 30 \
                --output "${PATCH_FILENAME}" "${patch}"
            git am < "$PATCH_FILENAME" 2>&1 | tee "${WORKDIR}/merge.log"

            # Get the patch subject from the patch email so we get the full patch
            # subject from the original email to the kernel mailing list.
            PATCH_SUBJECT=$(formail -q -z -x "Subject" < $PATCH_FILENAME | awk '{$1=$1};1' | paste -sd " " -)

            # Write the patch data (URL or filename) and subject to the rc file.
            if [ -z "${patchwork_url:-}" ]; then
                echo "localpatch_$(printf '%03d' ${COUNTER}) = $PATCH_FILENAME" >> "${RC_FILE}"
                echo "localpatch_$(printf '%03d' ${COUNTER})_subject = ${PATCH_SUBJECT}" >> "${RC_FILE}"
            else
                echo "patchwork_$(printf '%03d' ${COUNTER}) = ${patch}" >> "${RC_FILE}"
                echo "patchwork_$(printf '%03d' ${COUNTER})_subject = ${PATCH_SUBJECT}" >> "${RC_FILE}"
            fi
            (( COUNTER++ ))
        done
    fi

popd
