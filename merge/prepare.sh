#!/bin/bash
set -euxo pipefail
# Prepare for merging patches.

# Set up the path for locally installed python executables and ccache.
export PATH=${HOME}/.local/bin:/usr/lib64/ccache:${PATH}

# Set up some git identification.
git config --global user.name "CKI Pipeline"
git config --global user.email "cki-project@gitlab.com"

# Ensure RPM is happy with our UID/GID in OpenShift.
if [ -w '/etc/passwd' ]; then
    echo "cki:x:$(id -u):${id -g}:,,,:${HOME}:/bin/bash" >> /etc/passwd
fi
if [ -w '/etc/group' ]; then
    echo "cki:x:$(id -G | cut -d' ' -f 2)" >> /etc/group
fi

# Ensure the workdir exists.
mkdir -vp "${WORKDIR}"
