#!/bin/bash
set -euxo pipefail

# Get the tag of the current commit.
pushd "${WORKDIR}"
    TAG=$(git describe --abbrev=0 || true)
popd

if [[ $TAG =~ ^kernel ]]; then
    # Handle kernel tags in kernel-VERSION-RELEASE format.
    echo "tag = -$(cut -d '-' -f 3-)" <<< "${TAG}" | tee -a "${RC_FILE}"
elif [[ $TAG =~ ^[0-9] ]]; then
    # Handle kernel tags in VERSION-RELEASE format.
    echo "tag = -$(cut -d '-' -f 2-)" <<< "${TAG}" | tee -a "${RC_FILE}"
else
    # Handle upstream kernel tags, such as v5.0-rc8, or situations where
    # there are no tags at all.
    echo "tag = -$(git rev-list --max-count=1 HEAD | cut -b-7)" | tee -a "${RC_FILE}"
fi
