#!/bin/bash
set -euxo pipefail

# Make final updates to rc file.

# If we made it this far, then the merge was successful. We need to remove
# the 'mergelog' line from the rc file so that the reporter knows this merge
# was not a failure.
sed -i '/^mergelog = /d' "${RC_FILE}"
rm -fv "${WORKDIR}/merge.log"

# Store some additional data now that the merging is done.
pushd "${WORKDIR}"
    echo "test_hash = $(git log -1 --pretty=format:%H)" >> "${RC_FILE}"
popd

# Replace the original merge failure status in the rc file with a successful
# status.
sed -i 's/^stage_merge.*$/stage_merge = pass/' "$RC_FILE"
