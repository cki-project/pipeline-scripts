#!/bin/bash
set -euxo pipefail

# Package the source code into a source RPM.
if [[ $make_target == 'rpm' ]]; then
    pushd "${WORKDIR}"
        # Build source RPM. (Only Red Hat kernels supported.)
        if [[ $KERNEL_TYPE = "redhat" ]]; then
            # Fix cross compiler issue in RHEL 8.0.0 [BZ 1694956]
            curl -Ls --retry 5 --connect-timeout 30 \
                https://gitlab.cee.redhat.com/snippets/873/raw | git am || git am --abort
            if [[ $name =~ kernel-rt-rhel8 ]] ; then
                # Fix non-debug builds for RT
                curl -Ls --retry 5 --connect-timeout 30 \
                https://gitlab.cee.redhat.com/snippets/911/raw | git am || git am --abort
            fi
            echo ".cki" | tee localversion
            make rh-srpm
            mv redhat/rpm/SRPMS/*.src.rpm "${CI_PROJECT_DIR}/"
        else
            echo "RPM building is not implemented for upstream kernels."
            exit 1
        fi
    popd
fi
