#!/bin/bash
set -euxo pipefail

# Package the source code into a tarball.
if [[ $make_target == 'targz-pkg' ]]; then
    pushd "${WORKDIR}"
        # Build source tarball.
        if [[ $KERNEL_TYPE = "redhat" ]]; then
            # Use the built-in scripts to generate configs for RHEL kernels.
            make rh-configs
            mv redhat/configs/*.config .
            # NOTE(mhayden): RHEL kernels exclude anything under the 'redhat'
            # directory from appearing in git archives via the .gitattributes file.
            # If we want to keep that directory for builds, we must remove the
            # .gitattributes file.
            rm -fv .gitattributes
            git add .gitattributes
        else
            # Download configs from extracted Fedora configs for upstream kernels.
            CONFIG_ARCHES=( aarch64 ppc64le s390x x86_64 )
            for ARCH in "${CONFIG_ARCHES[@]}"; do
                curl -Ls --retry 5 --connect-timeout 30 --output "${ARCH}.config" \
                "https://gitlab.com/cki-project/kernel-configs/raw/master/${ARCH}.config"
            done
        fi
        # Add the config files to the git repo so git-archive will pick them up.
        git add -- *.config
        git commit -m "CKI - Adding config files"
        git archive -o "${CI_PROJECT_DIR}/kernel.tar.gz" HEAD
    popd
fi
