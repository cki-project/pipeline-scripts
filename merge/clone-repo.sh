#!/bin/bash
set -euxo pipefail

# Clone cached repo, update, check out ref.
protocol="$(grep :// <<< "$git_url" | sed -e's,^\(.*://\).*,\1,g')"
git_url_no_protocol=${git_url/$protocol/}
git clone --quiet --branch "${branch}" \
    "/opt/git-cache/${git_url_no_protocol}" "${WORKDIR}"

pushd "${WORKDIR}"
    git remote set-url origin "${git_url}"
    git remote -v
    git pull --rebase
    git checkout "${commit_hash}"
    echo "commit_message_title = $(git log -1 --pretty=format:%s)" >> "${RC_FILE}"
popd
